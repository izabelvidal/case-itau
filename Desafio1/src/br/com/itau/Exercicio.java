package br.com.itau;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import br.com.itau.modelo.Lancamento;
import br.com.itau.service.LancamentoService;

public class Exercicio {

	private static final DecimalFormat df = new DecimalFormat("0.00");
	public static void main(String[] args) {
		List<Lancamento> lancamentos = new LancamentoService().listarLancamentos();

		List<Lancamento> lancamentosByMes =
				lancamentos.stream().sorted(Comparator.comparing(c -> c.getMes())).collect(Collectors.toList());
		List<Lancamento> lancamentoByCategoria = lancamentos.stream().filter(c -> c.getCategoria() == 1).toList();
		Double total = lancamentos.stream().filter(c -> c.getMes() == 7).mapToDouble(c->c.getValor()).sum();

		System.out.println("by mes -> \n" + lancamentosByMes);
		System.out.println("by categoria -> \n" + lancamentoByCategoria);
		System.out.println("total -> \n" + df.format(total));

	}

}
