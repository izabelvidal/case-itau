package com.example.demo.service.mapper;

import com.example.demo.model.LancamentoModel;
import com.example.demo.model.dto.LancamentoDto;

public class LancamentoMapper {
    public static LancamentoDto toLancamentoDto(LancamentoModel model){
        return new LancamentoDto(model.getValor(), model.getOrigem(), model.getCategoria(), model.getMesLancamento());
    }
}
