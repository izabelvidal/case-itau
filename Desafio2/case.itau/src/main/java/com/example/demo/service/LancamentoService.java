package com.example.demo.service;

import com.example.demo.model.CategoriaModel;
import com.example.demo.model.LancamentoModel;
import com.example.demo.model.dto.CategoriaDto;
import com.example.demo.model.dto.LancamentoDto;
import com.example.demo.rest.LancamentoClient;
import com.example.demo.service.mapper.CategoriaMapper;
import com.example.demo.service.mapper.LancamentoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LancamentoService {

    @Autowired
    private LancamentoClient client;

    public List<CategoriaDto> listCategorias(){
        return client.getCategorias()
                .stream().map(CategoriaMapper::toCategoriaDto).collect(Collectors.toList());
    }

    public List<LancamentoDto> listLancamentos(){
        return client.getLancamentos()
                .stream().map(LancamentoMapper::toLancamentoDto).collect(Collectors.toList());
    }

    public List<CategoriaModel> listCategoriaById(Integer id){
        return client.getCategorias()
                .stream().filter(c -> c.getId() == id).collect(Collectors.toList());
    }

    public List<LancamentoModel> listLancamentoById(Integer id){
        return client.getLancamentos()
                .stream().filter(l -> l.getId() == id).collect(Collectors.toList());
    }
    public List<LancamentoModel> listLancamentosByCategoria(Integer categoria){
        return client.getLancamentos()
                .stream().filter(l -> l.getCategoria() == categoria)
                .collect(Collectors.toList());
    }
}
