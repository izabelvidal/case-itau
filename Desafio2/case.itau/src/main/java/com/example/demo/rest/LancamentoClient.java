package com.example.demo.rest;

import com.example.demo.model.CategoriaModel;
import com.example.demo.model.LancamentoModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "lancamentos", url = "https://desafio-it-server.herokuapp.com")
public interface LancamentoClient {

    @RequestMapping(method = RequestMethod.GET, value = "/lancamentos")
    List<LancamentoModel> getLancamentos();

    @RequestMapping(method = RequestMethod.GET, value = "/categorias")
    List<CategoriaModel> getCategorias();
}
