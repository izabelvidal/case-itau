package com.example.demo.controller;

import com.example.demo.model.CategoriaModel;
import com.example.demo.model.LancamentoModel;
import com.example.demo.model.dto.CategoriaDto;
import com.example.demo.model.dto.LancamentoDto;
import com.example.demo.service.LancamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/cartao")
public class LancamentoController {

    @Autowired
    private LancamentoService service;

    @GetMapping("/categorias")
    public ResponseEntity<List<CategoriaDto>> getCategorias(){
        List<CategoriaDto> retorno = service.listCategorias();
        return ResponseEntity.ok().body(retorno);
    }

    @GetMapping("/lancamentos")
    public ResponseEntity<List<LancamentoDto>> getLancamentos(){
        List<LancamentoDto> retorno = service.listLancamentos();
        return ResponseEntity.ok().body(retorno);
    }

    @GetMapping("/categorias/{id}")
    public ResponseEntity<List<CategoriaModel>> getCategoriasById(Integer id){
        List<CategoriaModel> retorno = service.listCategoriaById(id);
        return ResponseEntity.ok().body(retorno);
    }

    @GetMapping("/lancamentos/{id}")
    public ResponseEntity<List<LancamentoModel>> getLancamentosById(Integer id){
        List<LancamentoModel> retorno = service.listLancamentoById(id);
        return ResponseEntity.ok().body(retorno);
    }

    @GetMapping("/lancamentos/{categorias}")
    public ResponseEntity<List<LancamentoModel>> getLancamentosByCategoria(Integer categorias){
        List<LancamentoModel> retorno = service.listLancamentosByCategoria(categorias);
        return ResponseEntity.ok().body(retorno);
    }
}
