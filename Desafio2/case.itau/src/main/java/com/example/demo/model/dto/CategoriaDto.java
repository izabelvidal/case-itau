package com.example.demo.model.dto;

public class CategoriaDto {

    public CategoriaDto(String nome) {
        this.nome = nome;
    }

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
