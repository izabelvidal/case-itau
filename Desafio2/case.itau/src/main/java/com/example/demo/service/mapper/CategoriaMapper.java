package com.example.demo.service.mapper;

import com.example.demo.model.CategoriaModel;
import com.example.demo.model.dto.CategoriaDto;

public class CategoriaMapper {

    public static CategoriaDto toCategoriaDto(CategoriaModel model){
        return new CategoriaDto(model.getNome());
    }
}
