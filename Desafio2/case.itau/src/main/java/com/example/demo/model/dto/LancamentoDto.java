package com.example.demo.model.dto;

public class LancamentoDto {
    public LancamentoDto(Double valor, String origem, Integer categoria, Integer mesLancamento) {
        this.valor = valor;
        this.origem = origem;
        this.categoria = categoria;
        this.mesLancamento = mesLancamento;
    }

    private Double valor;
    private String origem;
    private Integer categoria;
    private Integer mesLancamento;

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    public Integer getMesLancamento() {
        return mesLancamento;
    }

    public void setMesLancamento(Integer mesLancamento) {
        this.mesLancamento = mesLancamento;
    }
}
